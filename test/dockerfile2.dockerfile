# 指定基础镜像
FROM golang:1.16-alpine

# 设置工作目录
WORKDIR /app

# 将应用程序代码复制到镜像中
COPY . .

# 构建应用程序
RUN go build -o myapp

# 指定容器启动时的命令
CMD ["./myapp"]
